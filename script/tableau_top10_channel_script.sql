SELECT
the_row as no
,app_name 
,Channel
,Title
,no_unique_user
,no_views
from 
(
SELECT 
app_name 
,event1  as Channel
,event2  as Title
,exact_count_distinct(ssoid) as no_unique_user
,count(*) as no_views
,ROW_NUMBER() OVER(PARTITION BY app_name order by no_unique_user desc ) the_row
FROM [true-dmp:DM_INTERNAL.tr_channel_event_play] 
where
(_PARTITIONTIME >=TIMESTAMP(<Parameters.Start Date>) and _PARTITIONTIME <= TIMESTAMP(<Parameters.End Date>))
and ssoid is not null
and event2 is not null
group by  1,2,3
)
where the_row <= 10
LIMIT 1000