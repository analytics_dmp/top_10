SELECT
app_name 
,display_name 
,event_action 
,movie_name 
,movie_type  
,count(ssoid) as no_views
,exact_count_distinct(ssoid) as no_unique_user
from (
SELECT
visit_start_time1
,ssoid
,app_name
,display_name
,event_action
,event_label
,case
    when REGEXP_MATCH (lower(event1), r'^vod_') 
            or REGEXP_MATCH (lower(event1), r'^tid_') 
            or REGEXP_MATCH (lower(event1), r'^svod_') then upper(event3)
    else upper(event1)
    end as movie_name
,movie_type
from
(
SELECT
visit_start_time1
,ssoid
,app_name
,display_name
,event_action
,event_label
,upper
    (case 
    when app_name = 'TrueID' and lower(event1) in ('tvod', 'svod') then event2
    else event1
    end)  as event1
,upper
    (case 
    when app_name = 'TrueID' and lower(event1) in ('tvod', 'svod') then event1
    else event2
    end) as event2
,event3
,upper
    (case 
    when app_name = 'TrueID' and (lower(event1) = 'tvod' or lower(event2) = 'tvod') and (event_action = 'Play Movie' or event_action = 'Play Series') then 'tvod'
    when app_name = 'TrueID' and (lower(event1) = 'svod' or lower(event2) = 'svod') and (event_action = 'Play Movie' or event_action = 'Play Series') then 'svod'
    when app_name = 'TrueTV' and lower(event2) = 'tvod' and event_action = 'Play Movie' then 'tvod'
    when app_name = 'TrueTV' and lower(event2) = 'svod' and event_action = 'Play Movie' then 'svod'
    else 'other'
    end) as movie_type
from
(
    SELECT
     visit_start_time1
    ,ssoid
    ,case 
        when hits.sourcePropertyInfo.sourcePropertyTrackingId in ('UA-86733131-3','UA-86733131-2','UA-86733131-1') then 'TrueID'
        when hits.sourcePropertyInfo.sourcePropertyTrackingId in ('UA-86733131-6','UA-86733131-5','UA-86733131-4') then 'TrueMusic'
        when hits.sourcePropertyInfo.sourcePropertyTrackingId in ('UA-86733131-9','UA-86733131-8') then 'HTV'
        when hits.sourcePropertyInfo.sourcePropertyTrackingId in ('UA-86733131-14','UA-86733131-15', 'UA-86733131-16') then 'TrueTV'
        else hits.sourcePropertyInfo.sourcePropertyTrackingId
    end as app_name
    ,case 
        when hits.sourcePropertyInfo.sourcePropertyDisplayName =  'TrueVideo for Android' then 'HTV for Android'
        when hits.sourcePropertyInfo.sourcePropertyDisplayName =  'TrueVideo for IOS' then 'HTV for IOS'
        else hits.sourcePropertyInfo.sourcePropertyDisplayName
    end as display_name
    ,hits.eventInfo.eventAction as event_action
    ,hits.eventInfo.eventLabel as event_label
    ,first(split(hits.eventInfo.eventLabel,',')) as event1
    ,nth(2,split(hits.eventInfo.eventLabel,',')) as event2
    ,nth(3,split(hits.eventInfo.eventLabel,',')) as event3
from
    (
	 select
    DATE_ADD(SEC_TO_TIMESTAMP(visitStartTime),7,"HOUR")as visit_start_time1
    ,fullvisitorid
    ,hits.customDimensions.value as ssoid
    ,hits.appInfo.screenName
    ,hits.appInfo.landingScreenName
    ,hits.eventInfo.eventAction
    ,hits.eventInfo.eventLabel
    ,hits.sourcePropertyInfo.sourcePropertyTrackingId
    ,hits.sourcePropertyInfo.sourcePropertyDisplayName
    from 
    TABLE_DATE_RANGE([133600662.ga_sessions_],
                    TIMESTAMP("2017-01-01"),
                    TIMESTAMP(DATE_ADD(CURRENT_DATE(),-1,'DAY')))
    where (hits.customDimensions.index = 1 or  hits.customDimensions.index is null)
    and hits.appInfo.screenName is not null
    and hits.appInfo.landingScreenName is not null
    and (hits.eventInfo.eventLabel is not null  or  hits.eventInfo.eventLabel not like'http://tvsedge147.truevisions.tv/%')
    and hits.eventInfo.eventAction = 'Play Movie' or hits.eventInfo.eventAction = 'Play Series'
    and hits.eventInfo.eventCategory in ('Player Interaction')
    group each by 1,2,3,4,5,6,7,8,9
    )
    )
)
)
group by 1,2,3,4,5