select 
the_row as no
,app_name
,Channel
,Title
,no_views
,no_users
from (

      SELECT
      app_name
      ,event1  as Channel
      ,event2 as Title
      ,count(*) as no_views
      ,exact_count_distinct(ssoid) as no_users
      ,ROW_NUMBER() OVER(PARTITION BY app_name  order by no_users desc ) the_row
      from (
SELECT  
    ssoid
    ,case 
        when hits.sourcePropertyInfo.sourcePropertyTrackingId in ('UA-86733131-3','UA-86733131-2','UA-86733131-1') then 'TrueID'
        when hits.sourcePropertyInfo.sourcePropertyTrackingId in ('UA-86733131-6','UA-86733131-5','UA-86733131-4') then 'TrueMusic'
        when hits.sourcePropertyInfo.sourcePropertyTrackingId in ('UA-86733131-9','UA-86733131-8') then 'HTV'
        when hits.sourcePropertyInfo.sourcePropertyTrackingId in ('UA-86733131-14','UA-86733131-15', 'UA-86733131-16') then 'TrueTV'
        else hits.sourcePropertyInfo.sourcePropertyTrackingId
    end as app_name
    ,case 
        when hits.sourcePropertyInfo.sourcePropertyDisplayName =  'TrueVideo for Android' then 'HTV for Android'
        when hits.sourcePropertyInfo.sourcePropertyDisplayName =  'TrueVideo for IOS' then 'HTV for IOS'
        else hits.sourcePropertyInfo.sourcePropertyDisplayName
    end as display_name
    ,hits.eventInfo.eventAction as event_action
    ,first(split(hits.eventInfo.eventLabel,',')) event1
    ,nth(2,split(hits.eventInfo.eventLabel,',')) event2
    ,nth(3,split(hits.eventInfo.eventLabel,',')) event3
from
    (
    select 
    hits.customDimensions.value as ssoid
    ,DATE_ADD(SEC_TO_TIMESTAMP(visitStartTime),7,"HOUR") as visit_start_time1
    ,hits.appInfo.screenName
    ,hits.appInfo.landingScreenName
    ,hits.eventInfo.eventCategory
    ,hits.eventInfo.eventAction
    ,hits.eventInfo.eventLabel
    ,hits.sourcePropertyInfo.sourcePropertyTrackingId
    ,hits.sourcePropertyInfo.sourcePropertyDisplayName
    from 
    TABLE_DATE_RANGE([133600662.ga_sessions_],
                    TIMESTAMP(<Parameters.Start Date>),
                    TIMESTAMP(<Parameters.End Date>))
    where (hits.customDimensions.index = 1 or  hits.customDimensions.index is null)
    and hits.appInfo.screenName is not null
    and hits.eventInfo.eventLabel is not null
    and hits.eventInfo.eventAction = 'Play TV'
    and  hits.sourcePropertyInfo.sourcePropertyTrackingId in ('UA-86733131-3','UA-86733131-2','UA-86733131-1','UA-86733131-14','UA-86733131-15', 'UA-86733131-16')
    group each by 1,2,3,4,5,6,7,8,9
    )
)
group by 1,2,3
)
where the_row <= 10