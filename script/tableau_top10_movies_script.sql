select 
the_row as no
,app_name 
,movie_name
,play_type as movie_type
,no_views
,no_unique_user
from
(
    SELECT
    app_name  
    ,event3 as  movie_name 
    ,play_type   
    ,count(*) as no_views
    ,exact_count_distinct(ssoid) as no_unique_user
    ,ROW_NUMBER() OVER(PARTITION BY app_name , play_type order by no_unique_user desc ) the_row
    FROM [true-dmp:DM_INTERNAL.TR_movie_event_play]
    WHERE
    _PARTITIONTIME >=TIMESTAMP(<Parameters.Start Date>)
    and _PARTITIONTIME <= TIMESTAMP(<Parameters.End Date>)
    group by 1,2,3
)
where the_row <= 10
