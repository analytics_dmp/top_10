SELECT
day_id
,ssoid
,fullVisitorId
,visit_start_time1
,app_name
,display_name
,screen_name
,landing_screen_nm
,event_cate
,event_action
,event_label
,case 
    when (app_name = 'TrueID' or app_name = 'TrueTV') and event_action = 'Play TV' then event1 
    else event1
    end  as Channel
,case
    when app_name = 'TrueID' and event_action = 'Play Exclusive' and (event2 = 'soccer_fullmatch' or event2 = 'soccer_live') then event3
    when app_name = 'TrueID' and event_action = 'Play Clip' and (event2 = 'sport' or event2 = 'soccer_preview') then event3
    when app_name = 'TrueID' and event1 = 'soccer_highlight' and event2 in ('EPL', 'FL1', 'J1', 'LFP','LSA', 'TPL', 'UEL') then event2 +  '  ' + event3
    else event2
    end as Title
,event3
from
(
    SELECT  
    day_id
    ,hits.customDimensions.value as ssoid
    ,fullVisitorId
    ,visit_start_time1
    ,case 
        when hits.sourcePropertyInfo.sourcePropertyTrackingId in ('UA-86733131-3','UA-86733131-2','UA-86733131-1') then 'TrueID'
        when hits.sourcePropertyInfo.sourcePropertyTrackingId in ('UA-86733131-6','UA-86733131-5','UA-86733131-4') then 'TrueMusic'
        when hits.sourcePropertyInfo.sourcePropertyTrackingId in ('UA-86733131-9','UA-86733131-8') then 'HTV'
        when hits.sourcePropertyInfo.sourcePropertyTrackingId in ('UA-86733131-14','UA-86733131-15', 'UA-86733131-16') then 'TrueTV'
        else hits.sourcePropertyInfo.sourcePropertyTrackingId
    end as app_name
    ,case 
        when hits.sourcePropertyInfo.sourcePropertyDisplayName =  'TrueVideo for Android' then 'HTV for Android'
        when hits.sourcePropertyInfo.sourcePropertyDisplayName =  'TrueVideo for IOS' then 'HTV for IOS'
        else hits.sourcePropertyInfo.sourcePropertyDisplayName
    end as display_name
	,hits.appInfo.screenName as screen_name
    ,hits.appInfo.landingScreenName as landing_screen_nm
    ,hits.eventInfo.eventCategory as event_cate
    ,hits.eventInfo.eventAction as event_action
    ,hits.eventInfo.eventLabel as event_label
    ,first(split(hits.eventInfo.eventLabel,',')) as event1
    ,nth(2,split(hits.eventInfo.eventLabel,',')) as event2
    ,nth(3,split(hits.eventInfo.eventLabel,',')) as event3
from
    (
	select 
    date as day_id
    ,hits.customDimensions.value
    ,fullVisitorId
    ,DATE_ADD(SEC_TO_TIMESTAMP(visitStartTime),7,"HOUR")as visit_start_time1
    ,hits.appInfo.name
    ,hits.appInfo.screenName
    ,hits.appInfo.landingScreenName
    ,hits.eventInfo.eventCategory
    ,hits.eventInfo.eventAction
    ,hits.eventInfo.eventLabel
    ,hits.sourcePropertyInfo.sourcePropertyTrackingId
    ,hits.sourcePropertyInfo.sourcePropertyDisplayName
    from 
    TABLE_DATE_RANGE([133600662.ga_sessions_],
                    TIMESTAMP("2017-01-01"),
                    TIMESTAMP(DATE_ADD(CURRENT_DATE(),-1,'DAY')))
    where hits.customDimensions.index = 1 or  hits.customDimensions.index is null
    group each by 1,2,3,4,5,6,7,8,9,10,11,12
    )
    where hits.appInfo.screenName is not null
    and hits.eventInfo.eventLabel is not null
    and hits.eventInfo.eventAction in ('Play TV','Play Clip','Play Exclusive')
  )
  where app_name in ('TrueID', 'TrueTV')


  